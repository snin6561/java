package com.home.work;

import java.util.Scanner;

public class Test2 {
	public static void main(String[] args) {
		int dice = 0;
		int [] diary = null;
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("===1.주사위 크기 2.주사위 굴리기 3.결과 보기 4.가장 많이 나온 수 5.종료===");
			System.out.println("메뉴 >");
			int selectMu = Integer.parseInt(sc.nextLine());
			
			if(selectMu == 1) {
				System.out.println("주사위 크기 >");
				dice = Integer.parseInt(sc.nextLine());
					if(dice < 5 || dice > 10) {
						System.out.println("범위를 벗어 남");
					}
				
			}else if(selectMu == 2) {
				diary = new int[dice];
				for(int i=0; i<diary.length; i++) {
					if(i == 5) {
						System.out.println("5가 나올 때 까지"+ i + "번 굴렸습니다");
					}
				}
				
			}else if(selectMu == 3) {
				for(int i=0; i<diary.length; i++) {
					System.out.println((i+1)+ "은"+ diary[i]+"번 나왔습니다");
				}
			}else if(selectMu == 4) {
				int max =0;
				int index = 0;
				for (int num : diary) {
					if(max<num) {
						max = num;
					}
				}
				for(int i=0; i<diary.length; i++) {
					if(max<diary[i]) {
						index = i;
					}
				}
			}else if(selectMu == 5) {
				System.out.println("프로그램 종료");
				break;
			}
		}
	}
}
