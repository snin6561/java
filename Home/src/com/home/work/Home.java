package com.home.work;

import java.util.Scanner;

public class Home {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	// 문제1) 차례대로 x와 y의 값이 주어졌을 때 어느 사분면에 해당되는지 출력하도록 구현하세요.
	// 각 사분면에 해당 하는 x와 y의 값은 아래를 참조하세요.
	// 제1사분면 : x>0, y>0
	// 제2사분면 : x<0, y>0
	// 제3사분면 : x<0, y<0
	// 제4사분면 : x>0, y<0
	// 문제출처, 백준(https://www.acmicpc.net/) 14681번 문제
		System.out.println("x와 y의 값을 입력");
		int no = Integer.parseInt(sc.nextLine());
		int no2 = Integer.parseInt(sc.nextLine());
		
		if(no>0 && no2>0) {
			System.out.println("제1사분면");
		}else if(no<0 && no2>0){
			System.out.println("제2사분면");
		}else if(no<0 && no2<0){
			System.out.println("제3사분면");
		}else if(no>0 && no2<0){
			System.out.println("제4사분면");
		}
	// 문제2) 연도가 주어졌을 때 해당 년도가 윤년인지를 확인해서 출력하도록 하세요.
	// 윤년은 연도가 4의 배수이면서 100의 배수가 아닐 때 또는 400의 배수일때입니다.
	// 예를 들어, 2012년은 4의 배수이면서 100의 배수가 아니라서 윤년이며,
	// 1900년은 100의 배수이고 400의 배수는 아니기 때문에 윤년이 아닙니다.
	// HiNT : 이중 IF문 사용
	// 문제출처, 백준(https://www.acmicpc.net/) 2753번 문제
		System.out.println("연도를 입력");
		int no3= Integer.parseInt(sc.nextLine());
		
		if(no3%4==0 && no3%100 != 0) {
			System.out.println("O");
			if(no3%400 == 0) {
			System.out.println("윤년이다");
			}
		} else {
			System.out.println("X");
		}
		
	// 문제3) 반복문을 활용하여 up & down 게임을 작성하시오. 1~100사이
	// 기회는 5번 이내로 맞추도록 하며, 맞추게 될 시에는 정답 공개 및 축하합니다.
	// 메세지 출력.
	// 사용자가 답안 제출 시, up, down이라는 메세지를 출력하면서 
	// 정답 유추를 할 수 있도록 한다.
		boolean flag = true;
		int random = (int)(Math.random()*100) + 1;
		System.out.println("up & down");
		 System.out.println(random);
		int no4 = 0;
			
		while(flag) {
			System.out.println("1~100사이 숫자를 입력");
			no4 = Integer.parseInt(sc.nextLine());
			int count = 1;	
			if (no4 > random) {
				System.out.println("Down");
				count++;
			}else if (no4 < random) {
				System.out.println("UP");
				count++;
			}else if (no4 == random) {
				System.out.println("정답, 축 하");
				break;
			}else if(count <= 5){
				System.out.println("5번초과");
				break;
			}
		}	
	// 문제4) 차례대로 m과 n을 입력받아 m단을 n번째까지 출력하도록 하세요.
	// 예를 들어 2와 3을 입력받았을 경우 아래처럼 출력합니다.
	// 2 X 1 = 2
	// 2 X 2 = 4
	// 2 X 3 = 6		

			System.out.println("곱하려는 수를 입력하세요");
			System.out.println("1");
			int nu1 = Integer.parseInt(sc.nextLine());
			System.out.println("2");
			int nu2 = Integer.parseInt(sc.nextLine());
			for(int i=1; i<=nu2; i++) {
				for(int j=0; j < i; j++) {
					System.out.println(nu1 + "x" + nu2 + "=" + (nu1 * nu2));
				}
			}
				
			
//			System.out.println(nu1 + "x" + nu2 + "=" + (nu1 * nu2));
	
		
	}
}
