package com.home.work;

import java.util.Scanner;

public class Home2 {
	public static void main(String[] args) {
	//주어진 배열을 이용하여 다음 내용을 구현하세요.
	int[] arr1 = { 10, 20, 30, 50, 3, 60, -3 };
				
	//문제1. 주어진 배열 중에서 값이 60인 곳의 인덱스를 출력해보자.
	int Index = 0;
	int no = 60;
	for(int i = 0; i<arr1.length; i++) {
		if(no == arr1[i]) {
			no = arr1[i];
			Index = i;
		}
		
	}System.out.println(Index);
				
	//문제2. 주어진 배열의 인덱스가 3인 곳은 출력하지 말고, 나머지만 출력해보자.
	for (int i=0; i<arr1.length; i++) {
		if(i != 3) {
			System.out.println(arr1[i]);
		}
	}
				
	//문제3. 주어진 배열 안의 변경하고 싶은 값의 인덱스 번호를 입력받아, 그 값을 1000으로 변경해보자.
	//   입력) 인덱스: 3 ->   {10, 20, 30, 1000, 3, 60, -3}
	Scanner sc = new Scanner(System.in);
	int Index2 = 0;
	int no2 = 0;
	System.out.println("인덱스 값을 입력>");
	Index2 = Integer.parseInt(sc.nextLine());
	System.out.println("값을 입력>");
	no2 = Integer.parseInt(sc.nextLine());
	arr1[Index2]=no2;
	for(int i=0; i<arr1.length; i++) {
		System.out.println(arr1[i]);
	}
	
	
				
	//문제4. 주어진 배열의 요소에서 최대값과 최소값을 구해보자.
	int max = arr1[0];
	for(int i=0; i<arr1.length; i++) {
		if(max < arr1[i]) {
			max = arr1[i];
		}
	}System.out.println(max);
	int min = arr1[0];
	for(int i=0; i<arr1.length; i++) {
		if(min > arr1[i]) {
			min = arr1[i];
		}
	}System.out.println(min);
				
	//문제5. 별도의 배열을 선언하여 양의 정수 10개를 입력받아 배열에 저장하고, 배열에 있는 정수 중에서 3의 배수만 출력해보자.
	int[] arr2 = new int[10];
	int[] arr3 = new int[10];
	System.out.println("양의 정수 10개를 입력하라");
	for(int i=0; i<arr2.length; i++) {
		arr2[i] = Integer.parseInt(sc.nextLine());
	}
	for(int i=0; i<arr2.length; i++) {
		if(arr2[i]%3 == 0) {
			arr3[i]=arr2[i];
		}
	}
	System.out.println("3의 배수는:" );
	for(int i=0; i<arr3.length; i++) {
		if(arr3[i] != 0) {
			System.out.println(arr3[i]);
		}
	}
	

	
	//추가문제
   // - Lotto 생성
   //1) 1~45 사이의 수를 랜덤으로 추출합니다.
   //2) 랜덤으로 추출한 번호는 중복이 되선 안됩니다.
   //3) 번호는 6개만 추출합니다.
	

	}
}
