package com.home.work2;

public class Culture {
	public String title;
	public int direcnum;
	public int accnum;
	public int dance;
	public int total;
	
	public Culture (String title, int direcnum, int accnum) {
		this.title = title;
		this.direcnum = direcnum;
		this.accnum = accnum;
	}
	 public void setTotalScore(int score) {
		this.dance++;
		this.total += score;
	}
	 
	public String getGrade() {
		int avg = total/dance;
		String grade = null;
		
		switch (avg) {
			case 1: 
				grade = "*";
				break;
			case 2: 
				grade = "**";
				break;
			case 3: 
				grade = "***";
				break;
			case 4: 
				grade = "****";
				break;
			case 5: 
				grade = "*****";
				break;
		}
//		for(int i = 0; i < avg; i++) {
//			grade += "*";
//		}
		return grade;
	}
}
