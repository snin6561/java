package com.home.work2;

public class EmpDept extends Employee{
	public String division;
	
	public EmpDept (String name,int income,String division) {
		super(name,income);
		this.division = division;
	}

	@Override
	public void getInformation() {
		System.out.println("이름 :" + name + " 연봉 :" + income + " 부서 :" + division);
	}

	@Override
	public void print() {
		System.out.println("수퍼클래스\n서브클래스");
	}

	public String getDivision() {
		return division;
	}
	
	
}
