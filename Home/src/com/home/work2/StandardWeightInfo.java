package com.home.work2;

public class StandardWeightInfo extends Human{

	public StandardWeightInfo(String name, int height, int kg) {
		super(name,height,kg);
	}
	public double getStandardWeight() {
		return (height - 100)*0.9;
	}
	@Override
	public void getInformation() {
		System.out.println("이름: " + name + "키: "+ height + "몸무게: " + kg +"표준체중: "+ getStandardWeight());
		//System.out.println("표준체중 %.1f 입니다. \n");
	}
	
}
