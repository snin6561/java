package com.home.work2;

public class Human {
	public String name;
	public int height;
	public int kg;
	
	public Human(String name, int height, int kg) {
		this.name = name;
		this.height = height;
		this.kg = kg;
	}
	
	public void getInformation() {
		System.out.println("이름: " + name + "키: "+ height + "몸무게: " + kg);
	}
}
