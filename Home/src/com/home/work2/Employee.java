package com.home.work2;

public class Employee {
	//필드
	public String name;
	public int income;
	//생성자
	public  Employee(String name, int income) {
		this.name = name;
		this.income = income;
	}
	//메소드
	public void getInformation() {
		System.out.println("이름 : " + name + "연봉 : " + income);
	}
	public void print() {
		System.out.println("수퍼클래스");
	}
	public String getName() {
		return name;
	}
	public int getIncome() {
		return income;
	}
	
	
}
