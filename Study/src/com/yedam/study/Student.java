package com.yedam.study;

public class Student {
//필드
	private String stname;
	private String stclass;
	private String grade;
	private int programing;
	private int dataBase;
	private int OS;
	
//생성자
	//클래스를 통한 객체를 생성할 때 첫번째로 수행하는 일들을 모아두는곳.
	//필드에 대한 데이터를 객체를 생성할때 초기화 알 예정이라면
	//this키워드를 황용해 필드 초기화 하면 됨.
	
//메소드
	public String getStname() {
		return stname;
	}

	public void setStname(String stname) {
		this.stname = stname;
	}

	public String getStclass() {
		return stclass;
	}

	public void setStclass(String stclass) {
		this.stclass = stclass;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public int getPrograming() {
		return programing;
	}
	//
	public void setPrograming(int programing) {
		if(programing <= 0) {
			this.programing = 0;
		}
		this.programing = programing;
	}

	public int getDataBase() {
		return dataBase;
	}

	public void setDataBase(int dataBase) {
		this.dataBase = dataBase;
	}

	public int getOS() {
		return OS;
	}

	public void setOS(int oS) {
		OS = oS;
	}
	
}
