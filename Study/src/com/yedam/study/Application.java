package com.yedam.study;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		// 두 개의 주사위를 던졌을때, 눈의 합이 6이되는 모든 경우의 출력하는 프로그램
		//5,1/4,2/3,3/24/15
		// a주사위 b주사위
		// a주사위가 1일때, b주사위가 1~6까지의 합을 구한다음 6이면 만족
		
		for(int i = 0; i<6; i++) {
			for(int j=0; j<6; j++) {
				if(i+j == 6) {
				 System.out.println(i+ " " +j);
				}
			}
		}
		
		//숫자를 하나 입력받아, 양수인지 음수인지 출력
		//단 0이면 0이라고 출력
		
        
		Scanner sc = new Scanner(System.in);
		int nu = Integer.parseInt(sc.nextLine());
		if(nu%2==0) {
		   System.out.println(nu + " 은 양수");
		}else if(nu%2==1) {
			System.out.println(nu + " 은 음수");
		}else if(nu==0) {
			System.out.println("0이다");
		}
		
			
		
		//정수 두개와 연산기호 1개를 입력받아 연산기호에 해당되는 계산을 수행하고 출력 
		int nu1 = Integer.parseInt(sc.nextLine());
		int nu2 = Integer.parseInt(sc.nextLine());
		String op = sc.nextLine();
		if(op.equals("+")) {
			 System.out.println(nu1 + nu2);
		}else if(op.equals("-")) {
			System.out.println(nu1 - nu2);
		}else if(op.equals("/")) {
			System.out.println(nu1 / nu2);
		}else if(op.equals("*")) {
			System.out.println(nu1 * nu2);
		}
	}
}
