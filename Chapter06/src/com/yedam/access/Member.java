package com.yedam.access;

public class Member {
	// 필드
	private String id;
	private String pw;
	private String name;
	private int age;
	
	
	// 생성자
	
	// 메소드 setter getter => 데이터의 무결성을 지키기 위해서
	public void setAge(int age) {
		if(age < 0) {
			System.out.println("잘못된 나이입니다.");
			this.age = 0;
			return; // void 타입은 return을 쓰지 않는다 하지만 return;의 경우에는 하려던 것을 멈추고 메소드를 멈추고 메소드를 호출한 곳으로 이동
		} else {
			this.age = age;
		}	
	}
	
	public int getAge() {
		age += 1;
		return age;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		if(id.length() < 8) {
			System.out.println("8글자 미만 입니다. 다시 입력해주세요.");
			return;
		}
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
