package com.yedam.access;

public class Singleton { // 단 하나만의 객체를 보장하는 기법
		// 정적 필드
		private static Singleton singleton = new Singleton();
		
		
		// 생성자
		private Singleton() {
			
		}
		
		
		// 정적 메소드
		public static Singleton getInstance() { // static을 불러오기 위해 static 메소드로 만들어줌
			return singleton;
		}
}
