package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		Access access = new Access();
		
		// public
		access.free = "free";
		
		// protected
		access.parent = "parent"; // 같은 패키지 or 부모자식
		
		// default
		access.basic = "basic"; // 같은 패키지
		
		//private
//		access.privacy = "privacy"; // 같은 클래스 내에서만
		
		access.free();
		
		Singleton obj1 = Singleton.getInstance();
		Singleton obj2 = Singleton.getInstance();
		
		if(obj1 == obj2) { // 참조변수의 ==은 주소비교
			System.out.println("같은 싱글톤 객체입니다.");
		} else {
			System.out.println("다른 객체");
		}
		
		
	}
}
