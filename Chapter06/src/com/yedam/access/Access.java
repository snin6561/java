package com.yedam.access;

public class Access {
	
	// 필드
	
	// 접근 제한자 = > 이름을 지어서 사용(변수, 클래스, 메소드 등등)
	// 1) public 어디서든 누구나 다 접근
	// 2) protected 같은 패키지 상속 받은 상태에서 부모 자식간의 사용(패키지가 달라도 사용 가능)
	// 3) default 패키지가 다른 사용 못함, 같은 패키지 내에서만
	// 4) private 내가 속한 클래스에서만 사용 가능
	public String free;
	protected String parent;
	String basic; // defualt 
	private String privacy;
	

	
	// 생성자
	public Access() {
	
	}
	
	private Access(String privacy) {
		this.privacy = privacy;
	}

	
	//메소드
//	public void run() {
//		System.out.println("달립니다.");
//	}
	
	public void free() {
		privacy();
		System.out.println("접근가능");
	}
	
	private void privacy() { // 기본적으로 접근불가능하지만 다른 메소드에서 호출함으로서 사용 가능
		System.out.println("접근이 불가능 합니다");
	}
	
	
}
