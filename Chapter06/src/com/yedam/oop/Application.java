package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		// SmartPhone 클래스(설계도)를 토대로 iphone14Pro를 만듬
		SmartPhone iphone14Pro = new  SmartPhone("Apple", "iphone12Pro", 500); // 생성자 : 클래스 변수 = new 클래스();
		iphone14Pro.maker = "Apple"; // 이렇게 직접 값을 넣어 줄수도 있고 생성자를 만들때 넣을수도 있음
		iphone14Pro.name = "iphone14Pro";
		iphone14Pro.price = 100000;
		
		iphone14Pro.price = 500; // 필드값은 덮어 쓸수 있음
		
		iphone14Pro.call();
		iphone14Pro.hangUp();
		
		// 필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		// SmartPhone 클래스(설계도) 
		
		SmartPhone zflip4 = new SmartPhone("samsung", "zflip4", 100000);
		zflip4.maker = "samsung";
		zflip4.name = "zflip4";
		zflip4.price = 100000;
		
		zflip4.call();
		zflip4.hangUp();
		
		SmartPhone sony = new SmartPhone();
		
		sony.getInfo(0); // return 타입이 없는 메소드
		int b = sony.getInfo("int"); // return 타입이 int인 메소드
		String[] temp = sony.getInfo(args); // return 타입이 String[]인 메소드
		
		
	}

}
