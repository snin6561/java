package com.yedam.oop;

public class Application5 {
	public static void main(String[] args) {
		Student std1 = new Student("김또치", "예담고", 221124);
		Student std2 = new Student("이또치", "예담고", 221124);
		Student std3 = new Student("박또치", "예담고", 221124);
		Student std4 = new Student("최또치", "예담고", 221124);
		Student std5 = new Student("정또치", "예담고", 221124);
		Student std6 = new Student("장또치", "예담고", 221124);
		
		Student[] stdAry = new Student[6];
		stdAry[0] = std1;
		stdAry[1] = std2;
		stdAry[2] = std3;
		stdAry[3] = std4;
		stdAry[4] = std5;
		stdAry[5] = std6;
		
		for(int i = 0; i < stdAry.length; i++) {
			stdAry[i].kor = 50;
			stdAry[i].math = 60;
			stdAry[i].eng = 100;
			stdAry[i].getInfo();
		}
	}
}
