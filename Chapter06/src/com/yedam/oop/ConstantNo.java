package com.yedam.oop;

public class ConstantNo {
	final double pi = 3.14159;
	final int earthRound = 46250;
	final int lightSpeed = 127000;
	
	static final double PI = 3.14159; // 관례적으로 대문자
	static final int EARTH_ROUND = 46250;
	static final int LIGHT_SPEED = 127000;
	
	
	public ConstantNo() {
//		this.pi = 10.0; // final이 들어간 변수는 재설정될수 없다
	}
}
