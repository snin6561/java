package com.yedam.oop;

import java.util.Scanner;

import com.yedam.access.Access; // 기본적으로 import com.yedam.access(패키지).Access(클래스이름);

public class Application7 {
	
	int speed;
	
	void run() {
		System.out.println(speed + "으로 달립니다.");
	}
	
	public static void main(String[] args) {
		
		Access ac = new Access();
		Scanner scanner = new Scanner(System.in); // ctrl + shift + o로 import 가능
		
//		int speed2 = speed; // 스태틱이 아닌 인스턴스로는 호출이 불가능
//		run(); // 마찬가지로 스태틱 메소드도 불가능
		
		Application7 app = new Application7(); // 인스턴스화(생성자를 통해 객체를 생성)
		app.speed = 10; // dot(.)연산자를 통해서 가져와서 사용
		app.run();
		
		Car mycar = new Car("포르쉐"); // Car 클래스 안의 내용을 복사해서 Application7에 준비
		Car yourcar = new Car("벤츠");
		
		mycar.run(); // 
		yourcar.run();
		
		// 정적 필드, 메소드를 부르는 방법
		// 정적 멤버가 속해 있는 클래스명. 필드 또는 메소드 명
		// 1) 정적 필드 가져오는 방법
		double piRatio = Calculator.pi;
		System.out.println(piRatio);
		int result = Calculator.plus(5, 6);
		System.out.println(result);
		
		// *중요*
		// 1) 모든 클래스에서 가져와서 사용 할 수 있다 => 공유의 개념
		// 2) 너무 남용해서 사용하면 메모리 누수(부족) 현상이 발생할 수 있다.
		// 3) 주의 할 점 
		// static은 아무 영역에서나 사용가능
		// static 메소드는 내부 인스턴스에서 사용 불가, this도 X
		// static 메소드를 외부에서 사용하려고 한다면 static이 붙은 필드 또는 메소드만 가능
		// static 붙이지 않고 사용하고 싶다면 해당 필드와 메소드가 속해 있는 클래스를 인스턴스화(생성자를 통해 생성)하여
		// 인스턴스 필드와 인스턴스 메소드를 dot(.)연산자를 통해 가져와서 사용
		Person p1 = new Person("123123-123456", "김또치");
		
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
//		p1.nation = "USA"; // final은 변경 불가
		
		System.out.println(5*5*ConstantNo.PI); // 클래스이름.변수이름
		
		
	}
}
