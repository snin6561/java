package com.yedam.oop;

public class Book {
	// 필드
	String bookName;
	String bookType = "학습서";
	int price;
	String bookMaker;
	String ISBN;
	
	
	// 생성자
	public Book(String bookName, int price, String bookMaker, String ISBN) {
		this.bookName = bookName;
		this.price = price;
		this.bookMaker = bookMaker;
		this.ISBN = ISBN;
		
	}

	// 메소드
	void getInfo() {
		System.out.println("책 이름 : " + bookName);
		System.out.println("# 내용");
		System.out.println("1) 종류 : " + bookType);
		System.out.println("2) 가격 : " + price);
		System.out.println("3) 출판사 : " + bookMaker);
		System.out.println("4) 도서번호 : " + ISBN);
		System.out.println();
	}
	
}
