package com.yedam.oop;

public class Application3 {
	public static void main(String[] args) {
		Book book1 = new Book("혼자 공부하는 자바", 24000, "한빛미디어", "yedam-001");
		Book book2 = new Book("이것이 리눅스다", 32000, "한빛미디어", "yedam-002");
		Book book3 = new Book("자바스크립트 파워북", 22000, "어포스트", "yedam-003");
		
		book1.getInfo();
		book2.getInfo();
		book3.getInfo();
	}
	


}
