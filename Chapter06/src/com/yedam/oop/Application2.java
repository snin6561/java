package com.yedam.oop;

public class Application2 {
	public static void main(String[] args) {
		Calculator c1 = new Calculator();
		
		int sumResult = c1.sum(1,  2);
		double subResult = c1.sub(10,  20);
		
		System.out.println(sumResult);
		System.out.println(subResult);
		String temp = c1.result("메소드 연습");
		System.out.println(temp); 
		System.out.println(c1.result("메소드 연습")); // 위의 두 줄을 한줄로 표현
		c1.result("메소드 연습");
		
		Computer myCom = new Computer(); // 생성자
		int result = myCom.sum(1,2,3); // sum 메소드
		System.out.println(result);
		result = myCom.sum(1,2,3,4,5,6);
		System.out.println(result);
		
		
	}
}
