package com.yedam.oop;

public class Student {
	// 필드
	String name;
	String school = "에담고등학교";
	int StudentNum;
	int kor;
	int math;
	int eng;
	
	
	// 생성자
	public Student(String name, String school, int StudentNum, int kor, int math, int eng) {
		this.name = name;
		this.school = school;
		this.StudentNum = StudentNum;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
	}
	
	public Student() {
		this.name = "김둘리";
		this.school = "예담고등학교";
		this.StudentNum = 221126;
		this.kor = 80;
		this.math = 60;
		this.eng = 70;
	}
	
	public Student(String name, int StudentNum, int kor, int math, int eng) {
		this.name = name;
		this.StudentNum = StudentNum;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
	}
	
	public Student(String name, String school, int StudentNum) {
		this.name = name;
		this.school = school;
		this.StudentNum = StudentNum;
	}
	
	
	// 메소드
	void getInfo() {
		System.out.println("학생의 이름 : " + name);
		System.out.println("학생의 학교 : " + school);
		System.out.println("학생의 학번 : " + StudentNum);
		sum();
		System.out.println("총 점 : " + sum1());
		avg();
		System.out.println("평 균 : " + avg1());
		System.out.println();
	}
	
	void sum() {
		int sum = kor + math + eng;
		System.out.println("총 점 : " + sum);
	}
	
	int sum1() { // 이렇게 해도됨
		return kor + math + eng;
	}
	
	void avg() {
		double avg = ((double)kor + math + eng) / 3;
		System.out.println("평 균 : " + avg);
	}
	
	double avg1() {
		double avgs = sum1() / (double)3;
		return avgs;
	}
	
	Student[] stdAry = new Student[6];
}
