package com.yedam.oop;

import com.yedam.access.Access;

public class Application8 {
	public static void main(String[] args) {
		Access access = new Access(); // import 단축키가 기억 안나면 Application7으로
		
		// public
		access.free = "free"; // 모두 가능
		// protected
		access.parent = "parent"; // 같은 패키지 or 부모자식 관계만 가능
		
		// default
		access.basic = "basic"; // 다른 패키지에서 사용 불가능
		
		//private
		access.privacy = "privacy"; // 내 자신이 아니면 모두 사용 불가
		
		
		
		
		
		
		
		
	}
}
