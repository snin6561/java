package com.yedam.oop;

public class Computer {
	// 필드
	
	// 생성자
	
	// 메소드
	int sum(int ... values) { // ... 매개 변수가 들어오면 values라는 1차원 배열이 됨
		int sum = 0;
		for(int i = 0; i < values.length; i++) {
			System.out.println(values[i]);
			sum += values[i];
		}
		return sum;
	}
}
