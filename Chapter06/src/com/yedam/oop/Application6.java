package com.yedam.oop;

import java.util.Scanner;

public class Application6 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean flag = true;
		int stdCount = 0;
		Student[] stdAry = null;
		
		
		while(flag) {
			System.out.println("1. 학생 수 | 2. 학생 정보 입력 | 3. 학생의 총점, 평균 | 4. 종료");
			System.out.println("입력>");
			int selectNo = Integer.parseInt(scanner.nextLine());
		
			if(selectNo == 1) {
				System.out.println("학생의 수 입력>");
				stdCount = Integer.parseInt(scanner.nextLine());
			} else if(selectNo == 2) {
				stdAry = new Student[stdCount];
				for(int i = 0; i < stdAry.length; i++) {
					Student std = new Student();
					System.out.println("학생 이름 입력>");
					String name = scanner.nextLine(); // 입력 받은 값을 이름에 넣는다 
					std.name = name; // 그 이름을 학생의 이름에 넣음
					System.out.println("국어 성적 입력>");
					int kor = Integer.parseInt(scanner.nextLine());
					std.kor = kor;
					System.out.println("수학 성적 입력");
					std.math = Integer.parseInt(scanner.nextLine());
					System.out.println("영어 성적 입력");
					std.eng = Integer.parseInt(scanner.nextLine());
					
					stdAry[i] = std; // 학생을 배열에 넣어줌
				}
			} else if(selectNo == 3) {
				for(int i = 0; i < stdAry.length; i++) {
					System.out.println(stdAry[i].name + " 학생 성적");
					System.out.println("총 점 : " + stdAry[i].sum1());
					System.out.println("평 균 : " + stdAry[i].avg1());
				}
			} else if(selectNo == 4) {
				System.out.println("종료합니다.");
				flag = false;
			} else {
				System.out.println("잘못된 입력입니다.");
			}
		}
	}
}
