package com.yedam.oop;

public class Application4 {
	public static void main(String[] args) {
		Student student1 = new Student("고길동", "예담고등학교", 221124, 100, 80, 90);
		Student student2 = new Student();
		Student student3 = new Student("김또치", 221126, 90, 50, 40);
		
		student1.getInfo();
		student2.getInfo();
		student3.getInfo();
		
		
	}
}
