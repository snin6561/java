package com.yedam.oop;

public class Calculator {
	// 필드
	
	
	// 정적 필드
	static double pi = 3.14;
	
	// 생성자(기본 생성자)
	
	
	// 정적 메소드
	static int plus(int x, int y) {
		return x + y;
	}
	
	// 메소드
	
	int sum(int a, int b) { // 오버로딩 기준점
		return a+b;
	}
	
	double sum (double a, double b) { // 매개 변수 타입 차이에 따른 오버로딩 int => double
		return a+b;
	}
	
	int sum(int a) { // 매개변수의 갯수에 따른 오버로딩 2개 => 1개
		return a;
	}
	
	double sum(double a, int b) { // 매개 변수의 데이터 타입 차이에 따른 오버로딩 int, int => double, int
		return a+b;
	}
	
	double sum(int a, double b) { // 매개 변수의 순서 차이에 따른 오버로딩 int, int => int, double 
		return a+b;
	}
	
	
	double sub(int a, int b) {
		return a-b; // int => double로 자동 변환
	}
	
	String result(String value) {
		String temp = "value return 테스트 : " + value;
		return temp;
	}
	
	void result1(String value) {
		System.out.println("value return 테스트 : " + value);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
