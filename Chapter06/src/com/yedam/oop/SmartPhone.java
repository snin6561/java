package com.yedam.oop;

public class SmartPhone {
	// 필드(객체의 정보를 저장)
	String name;
	String maker = "Apple"; // 초기값은 줄수도 있음(
	int price;
	
	// 생성자(클래스 이름과 똑같이 부여해서 만듬)
	public SmartPhone() { // 기본 생성자의 형태 public SmartPhone() {}
		
	}
	// 생성자가 클래스 내부에 "하나도 없다면" 알아서 기본 생성자를 만들고 객체를 생성
	
	public SmartPhone(String name) { // 오버로딩의 경우 매개변수에 따라서 다른 생성자로 인식함
		// 객체를 만들때 내가 원하는 행동 또는 데이터 저장 등등을 할대 여기에 내용을 구현
		this.name = name;
	}
	
	public SmartPhone(int price) { // 오버로딩의 경우 매개 변수의 갯수, 타입에 따라서 다른 생성자로 인식함
		this.price = price;
	}
	
	public SmartPhone(String name, int price) {
		this.name = name;
		this.price = price;
		
	}

	public SmartPhone(String name, String maker, int price) { // () 안에는 매개변수
		this.name = name; // this 이 클래스 안에 존재하는 name 즉 this = 해당 페이지(SmartPhone.java)
		this.maker = maker;
		this.price = price;
	}
	
	
	//메소드(객체의 기능을 정의)
	void call() {
		System.out.println(name + " 전화를 겁니다.");
	}
	void hangUp() {
		System.out.println(name + " 전화를 끊습니다.");
	}
	
	// 1) 리턴 타입이 없는 경우(void)
	void getInfo(int no) { // void 메소드이름(매개 변수)
		System.out.println("매개 변수 : " + no);
	}
	
	// 2) 리턴 타입이 있는 경우(기본타입(int double boolean), 참조 타입(String, 배열, 클래스)
	int getInfo(String temp) { // 기본 타입인 int를 리턴으로 받음
		
		return 0; // return은 무조건 int이여야함
	}
	
	String[] getInfo(String[] temp) { // 참조 타입인 String을 리턴으로 받음
		
		return temp; // return은 무조건 temp여야함
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
