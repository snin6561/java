package com.yedam.array;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//국영수
		int[] point = new int[3];
		System.out.println("국어>");
		point[0] = Integer.parseInt(sc.nextLine());
		
		System.out.println("영어>");
		point[1] = Integer.parseInt(sc.nextLine());
		
		System.out.println("수학>");
		point[2] = Integer.parseInt(sc.nextLine());
		//array.length
		//점수의 총합계, 평균
		int sum = 0;
		System.out.println("점수배열의 크기:" + point.length);
		for(int i=0; i<point.length; i++) {
			sum = sum + point[i];
		}
		System.out.println("점수의 총 합계:" + sum);
		
		double avg = (double)sum / point.length;
		System.out.println("점수의 평균:" + avg);
		
		
		
	}
}
