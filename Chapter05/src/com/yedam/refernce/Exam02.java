package com.yedam.refernce;

public class Exam02 {
	public static void main(String[] args) {
		String strVal1 = "yedam";//같은주소
		String strVal2 = "yedam";//이퀄즈 안써도 인식
		// 참조타입의 ==는 데이터를 비교하는게 아니라 데이터가 있는 메모리 주소를 비교***
		if(strVal1 == strVal2) {
			System.out.println("strVal1과 strVal2의 메모리 주소가 같다");
		}else {
			System.out.println("strVal1과 strVal2의 메모리 주소가 다르다");
		}
		//equals= 문자열(String)간의 데이터를 비교***
		if(strVal1.equals(strVal2)) {
			System.out.println("strVal1과 strVal2의 데이터는 같다");
		}else {
			System.out.println("strVal1과 strVal2의 데이터는 다르다");
		}
		
		String strVal3 = new String("yedam");
		String strVal4 = new String("yedam");
		
		if(strVal3 == strVal4) {
			System.out.println("strVal1과 strVal2는 같은 메모리 주소를 가지고 있다");
		}else {
			System.out.println("strVal1과 strVal2는 다른 메모리 주소를 가지고 있다");
		}
		
		if(strVal3.equals(strVal4)) {
			System.out.println("strVal1과 strVal2의 데이터는 같다");
		}else {
			System.out.println("strVal1과 strVal2의 데이터는 다르다");
		}
		
		if(strVal1 == strVal3) {
			System.out.println("strVal1과 strVal3는 같은 메모리 주소를 가지고 있다");
		}else {
			System.out.println("strVal1과 strVal3는 다른 메모리 주소를 가지고 있다");
		}
		
		
	}
}
