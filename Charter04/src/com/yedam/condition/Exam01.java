package com.yedam.condition;

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		// Math.random() -> 0 <= Math.random() <1
		// 0 * 60 <= Math.random() *60 < 1 * 60
		// 0 <= Math.random() <60
		// 0 + 40 <= Math.random() < 60 + 40
		// 40 <= Math.random() < 100, 40 ~ 99
		// 실수 -> 정수, casting
		int score = (int)(Math.random() *60) + 40;
		
		if(score >= 60) {
			System.out.println("합격");
		}else {
			System.out.println("불합격");
		}
		// 삼항 연산자
		String pass = (score >= 60) ? "합격" : "불합격";
		System.out.println(pass);
		//90이상 A, 70~80 B, 80~70 : C
		if (score >= 90) {
			pass = "A";
			} else {
			if(score >= 80) {
				pass = "B";
			} else {
			  if(score >= 70) {
			  pass = "C";
				} else {
					pass = "D";
				}
			}
		}	
		if (score >= 90) {
			pass = "A";
		}else if(score >= 80){
			pass = "B";
		}else if(score >= 70){
			pass = "C";
		}else {
			pass = "D";
		}
		//if 활용예제
		//사용자 입력값이 1,2,...9이면 "one""two"
//		Scanner sc = new Scanner(System.in);
//	
//		
//		System.out.println("숫자 입력");
//		int no = Integer.parseInt(sc.nextLine());
		
//		if (no == 1) {
//			pass = "one";
//		}else if (no == 2) {
//			pass = "two";
//		}else if (no == 3) {
//			pass = "th";
//		}else if (no == 4) {
//			pass = "fo";
//		}else if (no == 5) {
//			pass = "fi";
//		}else if (no == 6) {
//			pass = "si";
//		}else if (no == 7) {
//			pass = "se";
//		}else if (no == 8) {
//			pass = "ei";
//		}else if (no == 9) {
//			pass = "ni";
//		}else {
//			pass = "other";
//		}
//		System.out.println(pass);
		
//		switch(no) {
//		case 1: // ==
//			System.out.println("one");
//			break;
//		case 2:
//			System.out.println("two");
//			break;
//		case 3:
//			System.out.println("th");
//			break;
//		case 4:
//			System.out.println("fo");
//			break;
//		case 5:
//			System.out.println("fi");
//			break;
//		case 6:
//			System.out.println("si");
//			break;
//		case 7:
//			System.out.println("se");
//			break;
//		case 8:
//			System.out.println("ei");
//			break;
//		case 9:
//			System.out.println("ni");
//			break;
//		default: // else
//			System.out.println("other");
//			break;
//		}
		
		//break 문에 없는 switch문
		int time = (int)(Math.random()*4) + 8;
		System.out.println("현재 시각: " + time + "시");
		switch (time) {
		case 8: 
			System.out.println("출근을 합니다");
		case 9: 
			System.out.println("회의을 합니다");
		case 10: 
			System.out.println("업무을 합니다");
		default: 
			System.out.println("외근을 합니다");
		}
		
		//성적확인
		char grade= 'B';
		switch(grade) {
		case 'A':
			System.out.println("훌륭한 학생입니다");
			break;
		case 'B':
			System.out.println("우수한 학생입니다");
			break;
		case 'C':
			System.out.println("조금 노력하세요");
			break;
		case 'D':
			System.out.println("분발 하세요");
			break;
		}
		
		//문자열을 활용한 switch
		String position = "과장";
		switch (position){
			case "부장": 
				System.out.println("700");
				break;
			case "차장": 
				System.out.println("600");
				break;
			case "과장": 
				System.out.println("500");
				break;
			default: 
				System.out.println("300");
				break;
		}
		
		
		
    }
}