package com.yedam.condition;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		//학점 계산
		//switch문
		//사용자가 입력한 점수를 토대로 학점을 출려
		//90: A ,90~80:B, 80~70:c, 70~60:D 그외는 F
		
		Scanner sc = new Scanner(System.in);
		System.out.println("성적입력>");
		int score = Integer.parseInt(sc.nextLine());
		int point = score/10;
		switch(point) { 
		case 10: 
		case 9: 
			System.out.println("A");
		 	break;
		case 8: 
			System.out.println("B");
		 	break;
		case 7: 
			System.out.println("C");
		 	break;
		case 6: 
			System.out.println("D");
		 	break;
		default: 
			System.out.println("F");
		 	break;
		}
	}
}
