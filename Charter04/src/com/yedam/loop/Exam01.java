package com.yedam.loop;

public class Exam01 {
	public static void main(String[] args) {
		// *반목문 안의 규칙
		int sum = 0;
		//규칙
//		sum = sum + 1; //sum=0+1
//		sum = sum + 2; //sum=1+2
//		sum = sum + 3; //sum=3+3
//		sum = sum + 4; 
//		sum = sum + 5;
		//1~5 가지의 합을 구하는 반복문
		for (int i=1; i<=5; i++) {
			sum = sum + i;
		}
		
		//1~100사이 짝수 구하는 반복문
		// 2 4 6 8 10...나머지가 0
		// i % 2 == 0
		for(int i = 1; i<=100; i++) {
			if(i%2==0) {
				System.out.println(i);
			}
		}
		
		// 1~100홀수
		for(int i = 100; i >= 1 ; i--) {
			if(i % 2==1) {
				System.out.println(i);
			}
		}
		//1~100 사이 2의 배수또는 3의 배수(||)
		//1~100 사이 2의 배수이거나 3의 배수(&&) = 6의배수
		for(int i=1; i <= 100; i++) {
			if(i % 3 == 0 && i % 2 == 0) {
				System.out.println(i + "는 2나3의 배수입니다");
			}
		}
		//구구단 출력
		//2단 출력
		// 2*i = 
		for (int i = 1; i<10; i++) {
			System.out.println("2x"+ i + "=" + (2*i));
		}
		//for 안의 for문
		//초기화 식에 들어가는 변수 고려
		//구구단 출력(2~9)
		for(int i=2; i<=9; i++) {
			//i= 2일때 아래반복문은 9번 돌아감
			for(int j=1; j<=9; j++) {
				System.out.println(i+"*"+j+"="+(i*j));
			}
		}
		
		//*****
		//*****
		//*****
		//*****
		//*****
		
		for (int i=0; i<5; i++) {//한칸씩 내려갈떄
			String star ="";
			for (int j=0; j<5; j++) {//별만들기
				star = star + "*";//+ 연산자를 활용하여 *만듬
			}
			System.out.println(star);
		}
		
		//*
		//**
		//***
		//****
		//*****
		for(int i=0; i<5; i++) {
			String star = "";
			for (int j=0; i>=j; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}
		
		//*****
		//****
		//***
		//**
		//*
		for (int i = 0; i<5; i++) {
			String star = "";
			for (int j=5; i<j; j--) {
				star = star + "*";
			}
			System.out.println(star);
		}
		//    *
		//   **
		//  ***
		// ****
		//*****
		for (int i=4; i>=0; i--) {
			String star = "";
			for (int j=1; j<=5; j++) {
				star = star + "*";
			}
			System.out.println(star);
		}
	}
}
