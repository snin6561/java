package com.yedam.inter2;

public interface InterfaceC extends InterfaceA, InterfaceB{
	//InterfaceA,B 내용이담긴 인터페이스
	//A기능+ B기능+ C기능
	public void methodC();
}
