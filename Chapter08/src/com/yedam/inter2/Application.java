package com.yedam.inter2;

public class Application {
	public static void main(String[] args) {
		Vehicle v1 = new Bus();
//		drive(v1);
//		Vehicle v2 = new Taxi();
//		drive(v2);
		v1.run();
		//v1.checkFare();
		
		
		Bus bus = (Bus) v1; //자동타입변환 전제로 강제타입으로 변환 //casting
		bus.run();
		bus.checkFare();
		
		drive(new Bus());
		drive(new Taxi());
	}
	
	public static void drive(Vehicle vehicle) {
		if(vehicle instanceof Bus) {
			Bus bus = (Bus) vehicle;
			bus.checkFare();
		}
		vehicle.run();
	}
}
