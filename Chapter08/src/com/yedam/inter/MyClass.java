package com.yedam.inter;

public class MyClass {
	//필드
	//1.필드에 인터페이스 사용
	RemoteControl rc = new Television();
	
	//생성자
	public MyClass() {
		
	}
	//2.생성자 매개변수에 인터페이스 사용
	public MyClass(RemoteControl rc) {
		this.rc = rc;
		rc.turnOn();
		rc.turnOff();
	}
	//메소드
	//3.메소드 안에서 로컬변수로 사용
	public void method1() {
		RemoteControl rtc = new Audio();
		rtc.turnOn();
		rtc.setVolume(5);
	}
	
	//4.메소드 매개변수로 사용
	public void methodB(RemoteControl rtc) {
		rtc.turnOn();
		rtc.turnOff();
	}
}
