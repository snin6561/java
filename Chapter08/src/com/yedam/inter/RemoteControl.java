package com.yedam.inter;

public interface RemoteControl extends Searchable{
	//상수
	public static final int MAX_VOLUME = 10;
	public int MIN_VOLUME = 0;
	//추상메소드
	public void turnOn();
	public abstract void turnOff();
	public void setVolume(int volume);
}
