package com.yedam.inter;

public class SmartTV implements RemoteControl{
	private int volume;
	
	//Searchable
	@Override
	public void search(String url) {
		System.out.println(url + "을 검색합니다");
	}
	//RemoteControl
	@Override
	public void turnOn() {
		System.out.println("켭니다");
	}

	@Override
	public void turnOff() {
		System.out.println("끕니다");
	}

	@Override
	public void setVolume(int volume) {
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume =  RemoteControl.MAX_VOLUME;
		}else if (volume > RemoteControl.MIN_VOLUME) {
			this.volume =  RemoteControl.MIN_VOLUME;
		}else {
			this.volume = volume;
		}
		System.out.println("현재볼륨:" + volume);
	}

}
