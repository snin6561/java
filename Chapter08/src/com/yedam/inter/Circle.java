package com.yedam.inter;

public class Circle implements GetInfo{
	
	int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}
	@Override
	public void area() {
		System.out.println(3.14*radius*radius);
		
	}

	@Override
	public void round() {
		System.out.println(2*3.14*radius);
		
	}

}
