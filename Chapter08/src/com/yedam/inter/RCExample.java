package com.yedam.inter;

public class RCExample {
	public static void main(String[] args) {
		RemoteControl rc;
		
		rc = new SmartTV();
		//SmartTV class - > implements RemoteControl(+ Searchable)
		//RemoteControl(+Searchable) -> Searchable을 상속
		//RemoteControl
		rc.turnOn();
		rc.setVolume(40);
		rc.turnOff();
		
		//Searchable sc = new SmartTV();
		rc.search("www.google.com");
		
//		rc = new Audio();
//		
//		rc.turnOn();
//		rc.setVolume(5);
//		rc.turnOff();
				
	}
}
