package com.yedam.inter;

public class Television implements RemoteControl{
	//필드
	private int volume;
	//생성자
	
	//메소드
	@Override
	public void turnOn() {
		System.out.println("전원킴");
	}

	@Override
	public void turnOff() {
		System.out.println("전원끔");
	}

	@Override
	public void setVolume(int volume) {
		//최대볼룸 이상 대이터 들어올때
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
		}
		//최소볼룸 이하 대이터 들어올때
		else if(volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
		}else {
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨:"+ this.volume);
	}

	@Override
	public void search(String url) {
		// TODO Auto-generated method stub
		
	}

}
