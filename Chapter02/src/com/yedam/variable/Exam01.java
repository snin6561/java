package com.yedam.variable;

public class Exam01 {
	
	static int v4;
	
	public static void main(String[] args) {
		int value; //변수 초기화
		value = 10; //
		System.out.println(value);
		int value2= 20;
		
		int value3;
		//int result =value3 + 10;
		//변수 값 복사
		int x=3;
		int y=5;
		int temp;//임시
		//데이터 변경전
		System.out.println("x:" + x + "," + "y:" + y);
		
		temp=x;
		x=y;
		y=temp;
		//데이터 변경후
		System.out.println("x:" + x + "," + "y:" + y);
		
		int v1 = 15;
		
		if(v1 > 10) {
			int v2;
			v2 = v1 - 10;
		}
		//int v3 = v1 + v2 + 5;
	}
}
