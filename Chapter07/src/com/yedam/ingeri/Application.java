package com.yedam.ingeri;

public class Application {
	public static void main(String[] args) {
		Child child = new Child();
		
		child.lastName = "또치";
		child.age = 20;
		
		System.out.println("내이름 : "+ child.firstName + child.lastName);
		System.out.println("DNA : " + child.DNA );
		//parent 클래스 -> bloodtype 을  private설정
		//child 틀래스 -> parent믈래스의 bloodtype 사용x
		//System.out.println("혈액형 : " + child.bloodType);
		System.out.println("나이 : " + child.age);
		
		Child2 child2 = new Child2();
		child2.lastName = "희동";
		child2.bloodType = 'A';
		child2.age = 5;
		
		System.out.println("내이름 : "+ child2.firstName + child2.lastName);
		System.out.println("DNA : " + child2.DNA );
		System.out.println("혈액형 : " + child2.bloodType);
		System.out.println("나이 : " + child2.age);

	}
}
