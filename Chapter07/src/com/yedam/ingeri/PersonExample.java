package com.yedam.ingeri;

public class PersonExample {
	public static void main(String[] args) {
		Person p1 = new Person("고희동", "123456-9876541");
		//부모 클래스 필드호출
		//Person() 을 실행할때, 부모객체를 만들어
		//그안의 인스턴스 필드를 가져와 실행
		System.out.println("name:" + p1.name);
		System.out.println("ssn:" + p1.ssn);
		//Person() 실행시 자신이 가지고 있는 인스턴스 필드 실행
		System.out.println("age:" + p1.age);
	}
}
