package com.yedam.ingeri;

public class Parent {
	//부모클래스
	//1) 상속할 필드 정의
	//protected 다른 패키지 사용//public 
	protected String firstName = "Lee";
	protected String lastName;
	protected String DNA = "B";
	int age;
	//2) 상속 대상에서 제외
	private char bloodType = 'B';
	
	//메소드
	protected void method1() {
		System.out.println("parent class -> method1 Call");
	}
	public void method2() {	
		System.out.println("parent class -> method2 Call");
	}
}
