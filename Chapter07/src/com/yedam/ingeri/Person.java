package com.yedam.ingeri;

public class Person extends People {
	
	public int age;
	//자식 객체를 만들 때 생성자를 통해 만듬
	//super() 를 통해 부모객체 생성
	//여기서 super() 의미하는 것은 부모의 생성자를 호출
	//따라서 자식 객체를 만들게되면 부모객체도 만들어진다
	public Person(String name, String ssn) {
		super(name, ssn);
	}
	
	
}
