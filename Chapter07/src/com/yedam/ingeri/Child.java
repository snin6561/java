package com.yedam.ingeri;

public class Child extends Parent{
	public String lastName;
	public int age;
	
	
	//메소드 오버라이딩 예제
	@Override
	public void method1() {
		System.out.println("child class -> method1 Overrride");
	}
	public void method3() {
		System.out.println("child class -> method3 Call");
	}
	
}
