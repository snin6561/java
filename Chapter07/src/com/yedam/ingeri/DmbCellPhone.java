package com.yedam.ingeri;

public class DmbCellPhone extends CellPhone{
	//필드
	int channel;
	//생성자
	public DmbCellPhone(String model, String color, int channel) {
		this.model = model;
		this.color = color;
		this.channel = channel;
	}
	//메소드
	void turnOnDmb() {
		System.out.println("채널" + channel + "번 수신");
	}
	void turnOffDmb() {
		System.out.println("멈춤");
	}
}
