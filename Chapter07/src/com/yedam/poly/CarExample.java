package com.yedam.poly;

public class CarExample {
	public static void main(String[] args) {
		Car car = new Car();
		
		for(int i = 0; i<=5; i++) {
			int problemLoc = car.run();
			
			switch (problemLoc) {
			//frontLeftTire
			case 1:
				System.out.println("앞왼쪽 한국타이어로교환");
				//Tire = 부모클래스
				//HanKookTire = 자식클랫,
				//Tire frontLeftTire = new HanKookTire("앞왼쪽",15);
				car.frontLeftTire = new HanKookTire("앞왼쪽",15);
				break;
			case 2:
				System.out.println("앞오른쪽 금호타이어로교환");
				car.frontRightTire = new KumhoTire("앞오른쪽",15);
				break;
			case 3:
				System.out.println("뒤왼쪽 한국타이어로교환");
				car.backLeftTire = new HanKookTire("뒤왼쪽",15);
				break;
			case 4:
				System.out.println("뒤오른쪽 금호타이어로교환");
				car.backRightTire = new KumhoTire("뒤오른쪽",15);
				break;
			}
			System.out.println("=======================");
		}
	}
}
