package com.yedam.abs;

public class AnimalExample {
	public static void main(String[] args) {
		Cat cat = new Cat();
		cat.sound();
		
		System.out.println();
		
		Animal animal = new Cat(); //추상클래스를 활용해 자기자신객체 사용
		animal.sound();
		animal.breathe();
		System.out.println(animal.kind);
		
		animalSound(new Cat());
		
		//추상클래스는 클래스와 별반 다를수없다
		//차이점은 
		//1.자기자신을 객체로 만들지 못함
		//2.추상메소드가 존재하며 상속을 받개되면 추상메소드는 반드사 구현해야함
		//3.스스로 객체 (인스턴스)화가 되지 않아 자식 클래스를 통한 자동타입변환으로 구현
	}
	
	//매개변수를 활용한 자동타입변환
	public static void animalSound(Animal animal) {
		animal.sound();
	}
}
