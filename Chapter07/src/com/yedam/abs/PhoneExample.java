package com.yedam.abs;

public class PhoneExample {
	public static void main(String[] args) {
		//추상 클래스 인스턴스화 확인
		//Phone phone = new Phone("Owner");
		
		SmartPhone smartPhone = new SmartPhone("홍길동");
		
		smartPhone.tuenOn();
		smartPhone.tuenOff();
		
		smartPhone.internetSearch();
	}
	
	
}
