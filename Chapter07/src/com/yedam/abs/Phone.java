package com.yedam.abs;

public abstract class Phone {
	//필드
	public String owner;
	
	//생성자
	public Phone(String owner) {
		this.owner = owner;
	}
	//메소드
	public void tuenOn() {
		System.out.println("폰전원을 킵니다");
	}
	public void tuenOff() {
		System.out.println("폰전원을 끕니다");
	}
}
